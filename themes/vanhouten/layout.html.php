<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo config('blog.description')?> | This Blog is driven by mark2">
    <meta name="author" content="<?php echo config('author.name')?>">
    <link rel="shortcut icon" href="">

    <title><?php echo isset($title) ? _h($title) : config('blog.title') ?></title>
    <link rel="alternate" type="application/rss+xml" title="<?php echo config('blog.title')?>  Feed" href="<?php echo site_url()?>rss" />

    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro%3A400%2C400italic%2C700" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">

    <link href="<?php echo site_url()?>themes/vanhouten/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo site_url()?>themes/vanhouten/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo site_url()?>themes/vanhouten/assets/css/bootstrap-social.css" rel="stylesheet">

    <link href="<?php echo site_url()?>themes/vanhouten/assets/css/main.css" rel="stylesheet">

    <script src="<?php echo site_url()?>themes/vanhouten/assets/js/pace.min.js"></script>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->
  </head>

  <body>
    <div id="secondary" class="col-sm-3 sidebar" style="display: none;">
      <aside>
        <div class="about">
          <h4>About this blog</h4>
          <p><?php echo config('blog.description')?></p>
        </div>

        <div class="socialmedia">
          <h4>Social</h4>
          <div class="socials-sidebar">
            <a class="btn btn-facebook" href="https://www.facebook.com/USERNAME" title="Facebook">
              <i class="fa fa-facebook"></i>
            </a>
            <a class="btn btn-twitter" href="http://twitter.com/USERNAME" title="Twitter">
              <i class="fa fa-twitter"></i>
            </a>
            <a class="btn btn-github" href="https://github.com/USERNAME" title="GitHub">
              <i class="fa fa-github"></i>
            </a>
          </div>
        </div>

      </aside>
    </div>

    <div id="primary" class="col-sm-12">
      <div class="content">

        <header class="header">
          <hgroup class="pull-left">
            <h1 class="site-title">
              <a rel="home" title="<?php echo config('blog.title')?>" href="<?php echo site_url()?>">
                <i class="fa fa-lemon-o"></i> <?php echo config('blog.title')?>
              </a>
            </h1>
          </hgroup>
          <div id="togglesidebar" class="btn btn-primary pull-right">
            <i class="fa fa-bars"></i>
          </div>
        </header>

        <?php echo content()?>

        <footer class="footer">
          <p>© <?php echo config('blog.title')?> <?php echo date("Y"); ?>. All right reserved. | This blog is driven by <a href="http://mark2.llow.me" title="mark2">mark2</a></p>
          <div class="socials-footer">
            <a class="btn btn-facebook" href="https://www.facebook.com/USERNAME" title="Facebook">
              <i class="fa fa-facebook"></i>
            </a>
            <a class="btn btn-twitter" href="http://twitter.com/USERNAME" title="Twitter">
              <i class="fa fa-twitter"></i>
            </a>
            <a class="btn btn-github" href="https://github.com/USERNAME" title="GitHub">
              <i class="fa fa-github"></i>
            </a>
          </div>
        </footer>

      </div>
    </div>

    <script src="<?php echo site_url()?>themes/vanhouten/assets/js/jquery-1.10.2.min.js"></script>
    <script src="<?php echo site_url()?>themes/vanhouten/assets/js/custom.js"></script>
  </body>
</html>