        <section class="post">
          <header class="entry-header">
            <img class="entry-avatar" alt="<?php echo config('author.name')?>" height="52" width="52" src="<?php echo get_gravatar(config('author.email')); ?>">
            <h2 class="entry-title"><?php echo $p->title ?></h2>
            <p class="entry-meta">
              Posted on <?php echo date('d F Y', $p->date)?> | By <?php echo config('author.name')?>
            </p>
          </header>
          <div class="entry-description">
            <p>
               <?php echo $p->body?> 
            </p>
          </div>
        </section>
        
        <div class="comments">
          <div id="disqus_thread"></div>
          <script type="text/javascript">
					/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
					var disqus_shortname = '<?php echo config('disqus.user') ?>'; // required: replace example with your forum shortname
			
					/* * * DON'T EDIT BELOW THIS LINE * * */
					(function() {
						var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
						dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
						(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
					})();
			</script>
			
			<script type="text/javascript">
				/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
				var disqus_shortname = '<?php echo config('disqus.user') ?>'; // required: replace example with your forum shortname
			
				/* * * DON'T EDIT BELOW THIS LINE * * */
				(function () {
					var s = document.createElement('script'); s.async = true;
					s.type = 'text/javascript';
					s.src = '//' + disqus_shortname + '.disqus.com/count.js';
					(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
				}());
			</script>
          <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
          <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
        </div>