		<?php foreach($posts as $p):?>
        <section class="post">
          <header class="entry-header">
            <img class="entry-avatar" alt="<?php echo config('author.name')?>" height="52" width="52" src="<?php echo get_gravatar(config('author.email')); ?>">
            <h2 class="entry-title"><a href="<?php echo $p->url?>"><?php echo $p->title ?></a></h2>
            <p class="entry-meta">
              Posted on <?php echo date('d F Y', $p->date)?> | By <?php echo config('author.name')?>
            </p>
          </header>
          <div class="entry-description">
            <p>
              <?php echo short_desc($p->body, 500); ?>
            </p>
          </div>
        </section>
       <?php endforeach;?>
	   
       <?php if ($has_pagination['prev']):?>
       		<ul class="pagination">
            <li><a href="?page=<?php echo $page-1?>">&laquo; Newer</a></li>
           </ul>
        <?php endif;?>
        
        <?php if ($has_pagination['next']):?>
        	<ul class="pagination">
            <li><a href="?page=<?php echo $page+1?>">Older &raquo;</a></li>
           </ul>
        <?php endif;?>